namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        public Vendedor(string nome, string cpf, string email, string telefone)
        {
            Nome = nome;
            Cpf = cpf;
            Email = email;
            Telefone = telefone;
        }

        public int Id { get; set; }
        public String Nome { get; set; }
        public String Cpf { get; set; }
        public String Email { get; set; }
        public String Telefone { get; set; }
    }
}