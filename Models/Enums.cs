namespace tech_test_payment_api.Models
{
    public class Enums
    {
        public enum EStatusVenda
        {
            AguardandoPagamento,
            PagamentoAprovado,
            Canceleda,
            EnviadoParaTransportadora,
            Entregue
        }
    }
}