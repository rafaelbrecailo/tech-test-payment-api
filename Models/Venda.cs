using tech_test_payment_api.Models;


namespace tech_test_payment_api.Models
{
    public class Venda

    {
        public Venda()
        {
            Status = Enums.EStatusVenda.AguardandoPagamento;
        }

        public int Id { get; set; }
        public DateOnly Data { get; set; }
        public Vendedor Vendedor { get; set; }
        public Enums.EStatusVenda Status { get; set; }
        public Produto[] Produtos { get; set; }
    }

}