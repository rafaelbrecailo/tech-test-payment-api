using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(
            DbContextOptions<DatabaseContext> options
            ) : base(options)
        {

        }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Venda>(tabela =>
            {
                tabela.HasKey(v => v.Id);
            });

            builder.Entity<Vendedor>(tabela =>
            {
                tabela.HasKey(v => v.Id);
            });

            builder.Entity<Produto>(tabela =>
            {
                tabela.HasKey(p => p.Id);
            });
        }
    }
}