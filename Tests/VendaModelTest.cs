using Xunit;

namespace tech_test_payment_api.Tests
{
    public class VendaModelTest
    {
        [Fact]
        public void StatusAguardandoPagamentoAoCriarNovaVenda()
        {
            Venda NovaVenda = new Venda();
            Assert.Equal(Enums.EStatusVenda.AguardandoPagamento, NovaVenda.Status);
        }
    }
}